if not exist "mdpi" mkdir mdpi
if not exist "hdpi" mkdir hdpi
if not exist "xhdpi" mkdir xhdpi
if not exist "xxhdpi" mkdir xxhdpi
if not exist "xxxhdpi" mkdir xxxhdpi
if not exist "web" mkdir web
Inkscape.exe --file %1 --export-area-page --export-png "mdpi/%2" --export-width 48
Inkscape.exe --file %1 --export-area-page --export-png "hdpi/%2" --export-width 72
Inkscape.exe --file %1 --export-area-page --export-png "xhdpi/%2" --export-width 96
Inkscape.exe --file %1 --export-area-page --export-png "xxhdpi/%2" --export-width 144
Inkscape.exe --file %1 --export-area-page --export-png "xxxhdpi/%2" --export-width 192
Inkscape.exe --file %1 --export-area-page --export-png "web/%2" --export-width 512