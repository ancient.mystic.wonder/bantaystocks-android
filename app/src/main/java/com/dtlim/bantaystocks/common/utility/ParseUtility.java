package com.dtlim.bantaystocks.common.utility;

import com.dtlim.bantaystocks.data.model.Stock;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dale on 7/1/16.
 */
public class ParseUtility {
    private static final Gson gson = new Gson();
    private ParseUtility() {}

    public static String[] parseStockList(String stockList) {
        return stockList.trim().isEmpty() ? new String[]{} : stockList.split(",");
    }

    public static String encodeStockList(String[] stockList) {
        if(stockList==null || stockList.length <= 0) {
            return "";
        }
        StringBuilder encoded = new StringBuilder();
        for(int i=0; i<stockList.length; i++) {
            encoded.append(stockList[i]);
            encoded.append((i == stockList.length-1 ? "" : ","));
        }
        return encoded.toString();
    }

    public static Stock parseSingleStockFromJson(String json) {
        try {
            return gson.fromJson(json, Stock.class);
        }
        catch (Exception e) {
        }
        return null;
    }

    public static List<Stock> parseStockListFromJson(String json) {
        try {
            Type listType = new TypeToken<ArrayList<Stock>>(){}.getType();
            return gson.fromJson(json, listType);
        }
        catch (Exception e) {
        }
        return null;
    }
}
