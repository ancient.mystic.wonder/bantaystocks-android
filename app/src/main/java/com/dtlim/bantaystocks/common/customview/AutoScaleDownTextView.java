package com.dtlim.bantaystocks.common.customview;

import android.annotation.TargetApi;
import android.content.Context;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;

/**
 * Created by amw on 10/6/2016.
 */

public class AutoScaleDownTextView extends TextView {

    private TextPaint mOriginalPaint;

    public AutoScaleDownTextView(Context context) {
        super(context);
        initialize();
    }

    public AutoScaleDownTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public AutoScaleDownTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    @TargetApi(21)
    public AutoScaleDownTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initialize();
    }

    private void initialize() {

    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        super.setText(text, type);
        scaleDown(text);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        scaleDown(getText());
    }

    private void scaleDown(CharSequence text) {
        if(text == null || text.length() == 0) {
            return;
        }

        int targetWidth = getWidth() - getPaddingLeft() - getPaddingRight();
        if (targetWidth <= 0) {
            return;
        }

        if(mOriginalPaint == null) {
            mOriginalPaint = new TextPaint(getPaint());
        }

        StaticLayout layout;
        int linecount = 999;
        TextPaint paint = new TextPaint(mOriginalPaint);
        while(true) {
            layout = new StaticLayout(text, paint, targetWidth, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, true);
            linecount = layout.getLineCount();
            if(linecount > 1) {
                paint.setTextSize(paint.getTextSize() - 1);
            }
            else {
                break;
            }
        }
        setTextSize(TypedValue.COMPLEX_UNIT_PX, paint.getTextSize());
    }
}
