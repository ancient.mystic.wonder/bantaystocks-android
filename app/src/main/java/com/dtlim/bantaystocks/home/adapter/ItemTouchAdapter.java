package com.dtlim.bantaystocks.home.adapter;

/**
 * Created by dale on 8/22/16.
 */
public interface ItemTouchAdapter {
    void onItemSwipe(int position);
    void onItemMove(int fromPosition, int toPosition);
}
