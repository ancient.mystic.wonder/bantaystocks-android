package com.dtlim.bantaystocks.home.adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.animation.OvershootInterpolator;

import java.util.List;

/**
 * Created by dale on 8/19/16.
 */
public class HomeItemAnimator extends DefaultItemAnimator {

    private static final OvershootInterpolator OVERSHOOT_INTERPOLATOR = new OvershootInterpolator(4);

    @Override
    public boolean canReuseUpdatedViewHolder(@NonNull RecyclerView.ViewHolder viewHolder) {
        return true;
    }

    @NonNull
    @Override
    public ItemHolderInfo recordPreLayoutInformation(@NonNull RecyclerView.State state,
                                                     @NonNull RecyclerView.ViewHolder viewHolder,
                                                     int changeFlags,
                                                     @NonNull List<Object> payloads) {
        HomeItemHolderInfo holderInfo = new HomeItemHolderInfo();
        holderInfo.setFrom(viewHolder);
        return holderInfo;
    }

    @NonNull
    @Override
    public ItemHolderInfo recordPostLayoutInformation(@NonNull RecyclerView.State state,
                                                      @NonNull RecyclerView.ViewHolder viewHolder) {
        HomeItemHolderInfo holderInfo = new HomeItemHolderInfo();
        holderInfo.setFrom(viewHolder);
        return holderInfo;
    }

    @Override
    public boolean animateChange(@NonNull RecyclerView.ViewHolder oldHolder,
                                 @NonNull RecyclerView.ViewHolder newHolder,
                                 @NonNull ItemHolderInfo preInfo,
                                 @NonNull ItemHolderInfo postInfo) {
        final HomeItemHolderInfo homePreInfo = (HomeItemHolderInfo) preInfo;
        final HomeItemHolderInfo homePostInfo = (HomeItemHolderInfo) postInfo;
        HomeStocksAdapter.StockViewHolder holder = (HomeStocksAdapter.StockViewHolder) newHolder;

        if(!homePreInfo.holderPrice.equals(homePostInfo.holderPrice)) {
            animatePriceChange(holder);
        }
        return super.animateChange(oldHolder, newHolder, preInfo, postInfo);
    }

    private void animatePriceChange(final HomeStocksAdapter.StockViewHolder holder) {
        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator bounceAnimX = ObjectAnimator.ofFloat(holder.textViewPrice, "scaleX", 0.5f, 1f);
        bounceAnimX.setDuration(200);
        bounceAnimX.setInterpolator(OVERSHOOT_INTERPOLATOR);

        ObjectAnimator bounceAnimY = ObjectAnimator.ofFloat(holder.textViewPrice, "scaleY", 0.5f, 1f);
        bounceAnimY.setDuration(200);
        bounceAnimY.setInterpolator(OVERSHOOT_INTERPOLATOR);

        animatorSet.play(bounceAnimX).with(bounceAnimY);

        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                dispatchAnimationFinished(holder);
            }
        });
        animatorSet.start();
    }

    public static class HomeItemHolderInfo extends ItemHolderInfo {
        public String holderPrice;

        @Override
        public ItemHolderInfo setFrom(RecyclerView.ViewHolder holder) {
            HomeStocksAdapter.StockViewHolder viewHolder = (HomeStocksAdapter.StockViewHolder) holder;
            this.holderPrice = viewHolder.textViewPrice.getText().toString();
            return super.setFrom(holder);
        }
    }
}
