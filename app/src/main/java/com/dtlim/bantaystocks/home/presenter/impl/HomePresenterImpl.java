package com.dtlim.bantaystocks.home.presenter.impl;

import com.dtlim.bantaystocks.common.utility.ParseUtility;
import com.dtlim.bantaystocks.data.database.repository.DatabaseRepository;
import com.dtlim.bantaystocks.data.model.Stock;
import com.dtlim.bantaystocks.data.repository.SharedPreferencesRepository;
import com.dtlim.bantaystocks.home.presenter.HomePresenter;
import com.dtlim.bantaystocks.home.view.HomeView;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by dale on 7/1/16.
 */
public class HomePresenterImpl implements HomePresenter, SharedPreferencesRepository.Listener {

    private HomeView mHomeView;
    private DatabaseRepository mDatabaseRepository;
    private SharedPreferencesRepository mSharedPreferencesRepository;

    private Subscription mSubscription;

    public HomePresenterImpl(DatabaseRepository databaseRepository,
                             SharedPreferencesRepository sharedPreferencesRepository) {
        mDatabaseRepository = databaseRepository;
        mSharedPreferencesRepository = sharedPreferencesRepository;
    }

    @Override
    public void bindView(HomeView view) {
        mHomeView = view;
    }

    @Override
    public void onActivityResume() {
        initializeData();
        mSharedPreferencesRepository.registerSharedPreferencesListener(this);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onActivityPause() {
        mSharedPreferencesRepository.unregisterSharedPreferencesListener(this);
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onActivityStop() {
        if(mSubscription != null && !mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
    }

    @Override
    public void initializeData() {
        setStocksFromSharedPreferences();
        setWatchedStocksFromSharedPreferences();
    }

    @Override
    public void watchStock(Stock stock) {
        String[] list = ParseUtility.parseStockList(mSharedPreferencesRepository.getWatchedStocks());
        ArrayList<String> stocks = new ArrayList<>(Arrays.asList(list));
        if(stocks.contains(stock.getSymbol())) {
            stocks.remove(stock.getSymbol());
        }
        else {
            stocks.add(0, stock.getSymbol());
        }
        mSharedPreferencesRepository.saveWatchedStocks(stocks.toArray(new String[stocks.size()]));
        setWatchedStocksFromSharedPreferences();
    }


    @Override
    public void subscribeToStock(Stock stock) {
        String[] list = ParseUtility.parseStockList(mSharedPreferencesRepository.getSubscribedStocks());
        ArrayList<String> stocks = new ArrayList<>(Arrays.asList(list));
        stocks.add(0, stock.getSymbol());
        mSharedPreferencesRepository.saveSubscribedStocks(stocks.toArray(new String[stocks.size()]));
        setStocksFromSharedPreferences();
    }

    @Override
    public void removeStock(Stock stock) {
        String[] list = ParseUtility.parseStockList(mSharedPreferencesRepository.getSubscribedStocks());
        ArrayList<String> stocks = new ArrayList<>(Arrays.asList(list));
        stocks.remove(stock.getSymbol());
        mSharedPreferencesRepository.saveSubscribedStocks(stocks.toArray(new String[stocks.size()]));
        setStocksFromSharedPreferences();

        mHomeView.showStockRemovedSnackbar(stock);
    }

    @Override
    public void moveStock(int from, int to) {
        String[] list = ParseUtility.parseStockList(mSharedPreferencesRepository.getSubscribedStocks());
        ArrayList<String> stocks = new ArrayList<>(Arrays.asList(list));
        if (from < to) {
            for (int i = from; i < to; i++) {
                Collections.swap(stocks, i, i + 1);
            }
        }
        else {
            for (int i = from; i > to; i--) {
                Collections.swap(stocks, i, i - 1);
            }
        }
        mSharedPreferencesRepository.saveSubscribedStocks(stocks.toArray(new String[stocks.size()]));
    }

    @Subscribe
    public void onEvent(Throwable throwable) {
        Timber.e(throwable, null);
        if(throwable instanceof MqttException) {
            mHomeView.showMessage("An error has occurred.  Trying to connect again...");
        }
        else {
            mHomeView.showMessage("An error has occurred.  Trying to connect again...");
        }
    }

    private void setStocksFromSharedPreferences() {
        String subscribedStocks = mSharedPreferencesRepository.getSubscribedStocks();
        String[] subscribedStocksList = ParseUtility.parseStockList(subscribedStocks);

        if(mSubscription != null) {
            mSubscription.unsubscribe();
        }

        Observable<List<Stock>> stocks = mDatabaseRepository.queryStocks(subscribedStocksList);
        mSubscription = stocks.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<Stock>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mHomeView.showMessage("An error has occurred.  Trying again...");
                    }

                    @Override
                    public void onNext(List<Stock> stocks) {
                        setStocksFromDatabase(stocks);
                    }
                });
    }

    private void setWatchedStocksFromSharedPreferences() {
        String watchedStocks = mSharedPreferencesRepository.getWatchedStocks();
        String[] watchedStocksList = ParseUtility.parseStockList(watchedStocks);
        mHomeView.setWatchedStocks(watchedStocksList);
    }

    private void setStocksFromDatabase(List<Stock> stocks) {
        if(stocks != null && !stocks.isEmpty()) {
            String[] list = ParseUtility.parseStockList(mSharedPreferencesRepository.getSubscribedStocks());
            ArrayList<String> stockSymbols = new ArrayList<>(Arrays.asList(list));
            List<Stock> displayList = new ArrayList<Stock>(stockSymbols.size());

            for(int i=0; i<stockSymbols.size(); i++) {
                String currentSymbol = stockSymbols.get(i);
                for(int j=0; j<stocks.size(); j++) {
                    Stock currentStock = stocks.get(j);
                    if(currentStock.getSymbol().equals(currentSymbol)) {
                        displayList.add(currentStock);
                        break;
                    }
                }
            }
            mHomeView.setSubscribedStocks(displayList);
            mHomeView.hideNoSubscribedStocks();
        }
        else {
            mHomeView.setSubscribedStocks(Collections.EMPTY_LIST);
            mHomeView.showNoSubscribedStocks();
        }
    }

    @Override
    public void onPreferenceChanged() {
        setWatchedStocksFromSharedPreferences();
    }
}
