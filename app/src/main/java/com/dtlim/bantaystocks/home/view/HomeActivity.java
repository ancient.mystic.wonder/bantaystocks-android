package com.dtlim.bantaystocks.home.view;

import android.annotation.TargetApi;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.dtlim.bantaystocks.BantayStocksApplication;
import com.dtlim.bantaystocks.BuildConfig;
import com.dtlim.bantaystocks.R;
import com.dtlim.bantaystocks.data.database.repository.DatabaseRepository;
import com.dtlim.bantaystocks.data.model.Stock;
import com.dtlim.bantaystocks.data.repository.SharedPreferencesRepository;
import com.dtlim.bantaystocks.grid.view.StocksGridActivity;
import com.dtlim.bantaystocks.home.adapter.HomeItemAnimator;
import com.dtlim.bantaystocks.home.adapter.HomeStocksAdapter;
import com.dtlim.bantaystocks.home.presenter.HomePresenter;
import com.dtlim.bantaystocks.home.presenter.impl.HomePresenterImpl;
import com.dtlim.bantaystocks.select.view.SelectStocksActivity;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity implements HomeView, HomeStocksAdapter.Listener {

    @BindView(R.id.bantaystocks_home_button_grid)
    TextView mButtonGrid;
    @BindView(R.id.bantaystocks_home_layout_not_subscribed)
    ViewGroup mViewNotSubscribed;
    @BindView(R.id.bantaystocks_home_button_subscribe)
    Button mButtonSubscribe;
    @BindView(R.id.bantaystocks_toolbar)
    Toolbar mToolbar;
    @BindView(R.id.bantaystocks_main_recyclerview)
    RecyclerView mRecyclerView;
    @BindView(R.id.bantaystocks_main_fab)
    FloatingActionButton mFloatingActionButton;
    @BindView(R.id.bantaystocks_home_adview)
    AdView mAdView;

    private HomeStocksAdapter mAdapter;
    private HomePresenter mHomePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.bantaystocks_activity_home);
        ButterKnife.bind(this);

        initializeToolbar();
        initializeList();
        initializePresenter();
        initializeListeners();
        initializeAds();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mHomePresenter != null) {
            mHomePresenter.onActivityStop();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mHomePresenter != null) {
            mHomePresenter.onActivityResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mHomePresenter != null) {
            mHomePresenter.onActivityPause();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    private void initializeToolbar() {
        if(mToolbar != null) {
            setSupportActionBar(mToolbar);
        }
        mButtonGrid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startStocksGridActivity();
            }
        });
    }

    private void initializeList() {
        mAdapter = new HomeStocksAdapter(this);
        mAdapter.setListener(this);
        mAdapter.setHasStableIds(true);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setItemAnimator(new HomeItemAnimator());

        initializeItemTouchHelper();
    }

    private void initializeItemTouchHelper() {
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.Callback() {
            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
                int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
                return makeMovementFlags(dragFlags, swipeFlags);
            }

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                mAdapter.onItemMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                mHomePresenter.moveStock(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                mAdapter.onItemSwipe(viewHolder.getAdapterPosition());
            }
        });
        itemTouchHelper.attachToRecyclerView(mRecyclerView);
    }

    private void initializePresenter() {
        DatabaseRepository databaseRepository = BantayStocksApplication.getDatabaseRepository();
        SharedPreferencesRepository sharedPreferencesRepository =
                BantayStocksApplication.getSharedPreferencesRepository();
        mHomePresenter = new HomePresenterImpl(databaseRepository, sharedPreferencesRepository);
        mHomePresenter.bindView(this);
    }

    private void initializeListeners() {
        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSelectStocksActivity();
            }
        });
        mButtonSubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startSelectStocksActivity();
            }
        });
    }

    private void initializeAds() {
        MobileAds.initialize(this, BuildConfig.ADMOB_PUBLISHER_ID);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("4F03300E63121D77796B471B5A4DFC68") // cherry g1
                .addTestDevice("C1BACC051CC9F62EA4BA161FFD5B6D62") // shamesung SM-G313HZ
                .build();
        mAdView.loadAd(adRequest);
    }

    @Override
    public void setSubscribedStocks(List<Stock> stocks) {
        mAdapter.setStockList(stocks);
    }

    @Override
    public void setWatchedStocks(String[] stocks) {
        mAdapter.setWatchedStocks(stocks);
    }

    @Override
    public void showNoSubscribedStocks() {
        mViewNotSubscribed.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideNoSubscribedStocks() {
        mViewNotSubscribed.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showStockRemovedSnackbar(final Stock stock) {
        Snackbar.make(mRecyclerView, "Unsubscribed from " + stock.getSymbol(), Snackbar.LENGTH_LONG)
                .setAction("UNDO", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mHomePresenter.subscribeToStock(stock);
                        Snackbar.make(mRecyclerView, "Subscribed to " + stock.getSymbol(), Snackbar.LENGTH_SHORT).show();
                    }
                })
                .show();
    }

    @Override
    public void onClickWatch(Stock stock) {
        attemptToShowHomescreenTicker(stock);
    }

    @Override
    public void onClickRemove(Stock stock) {
        mHomePresenter.removeStock(stock);
    }

    // https://developer.android.com/reference/android/Manifest.permission.html#SYSTEM_ALERT_WINDOW
    private void attemptToShowHomescreenTicker(Stock stock) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(Settings.canDrawOverlays(this)) {
                mHomePresenter.watchStock(stock);
            }
            else {
                requestDrawOverOtherAppsPermission();
            }
        }
        else {
            mHomePresenter.watchStock(stock);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void requestDrawOverOtherAppsPermission() {
        String packageName = getApplicationContext().getPackageName();
        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                Uri.parse("package:" + packageName));
        startActivity(intent);
    }

    private void startSelectStocksActivity() {
        Intent intent = new Intent(HomeActivity.this, SelectStocksActivity.class);
        startActivity(intent);
    }

    private void startStocksGridActivity() {
        Intent intent = new Intent(HomeActivity.this, StocksGridActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.activity_enter_from_right, R.anim.activity_exit_to_left);
    }
}
