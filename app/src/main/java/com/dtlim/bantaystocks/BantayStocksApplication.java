package com.dtlim.bantaystocks;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.dtlim.bantaystocks.analytics.AnalyticsManager;
import com.dtlim.bantaystocks.data.database.repository.DatabaseRepository;
import com.dtlim.bantaystocks.data.repository.SharedPreferencesRepository;
import com.dtlim.bantaystocks.data.repository.StocksNotificationRepository;
import com.dtlim.bantaystocks.data.service.StocksNotificationService;
import com.dtlim.bantaystocks.home.view.HomeActivity;
import com.facebook.stetho.Stetho;

import timber.log.Timber;

/**
 * Created by dale on 6/23/16.
 */
public class BantayStocksApplication extends Application {

    private static DatabaseRepository sDatabaseRepository;
    private static SharedPreferencesRepository sSharedPreferencesRepository;
    private static StocksNotificationRepository sStocksNotificationRepository;
    private static AnalyticsManager sAnalyticsManager;
    private static BackgroundManager sBackgroundManager;

    @Override
    public void onCreate() {
        super.onCreate();
        initialize();
        startStocksNotificationService();
    }

    public void initialize() {
        sDatabaseRepository = Injection.provideDatabaseRepository(this);
        sSharedPreferencesRepository = Injection.provideSharedPreferencesRepository(this);
        sStocksNotificationRepository = Injection.provideStocksNotificationRepository();
        sSharedPreferencesRepository.saveWatchedStocks(new String[] {});
        sAnalyticsManager = Injection.provideAnalyticsManager(this);
        sBackgroundManager = new BackgroundManager(this);

        Stetho.initializeWithDefaults(this);
        if(BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    public void startStocksNotificationService() {
        Intent intent = new Intent(this, StocksNotificationService.class);
        startService(intent);
    }

    public static DatabaseRepository getDatabaseRepository() {
        return sDatabaseRepository;
    }

    public static SharedPreferencesRepository getSharedPreferencesRepository() {
        return sSharedPreferencesRepository;
    }

    public static StocksNotificationRepository getStocksNotificationRepository() {
        return sStocksNotificationRepository;
    }

    public static AnalyticsManager getAnalyticsManager() {
        return sAnalyticsManager;
    }

    public static boolean isAppInBackground() {
        return sBackgroundManager.isAppInBackground();
    }
}
