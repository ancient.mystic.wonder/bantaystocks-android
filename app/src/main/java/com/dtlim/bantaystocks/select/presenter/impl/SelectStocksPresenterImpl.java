package com.dtlim.bantaystocks.select.presenter.impl;

import com.dtlim.bantaystocks.analytics.AnalyticsManager;
import com.dtlim.bantaystocks.common.utility.ParseUtility;
import com.dtlim.bantaystocks.data.database.repository.DatabaseRepository;
import com.dtlim.bantaystocks.data.model.Stock;
import com.dtlim.bantaystocks.data.repository.SharedPreferencesRepository;
import com.dtlim.bantaystocks.select.presenter.SelectStocksPresenter;
import com.dtlim.bantaystocks.select.view.SelectStocksView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by dale on 7/4/16.
 */
public class SelectStocksPresenterImpl implements SelectStocksPresenter {
    private SelectStocksView mSelectView;
    private DatabaseRepository mDatabaseRepository;
    private SharedPreferencesRepository mSharedPreferencesRepository;
    private AnalyticsManager mAnalyticsManager;

    private Subscription mSubscription;

    public SelectStocksPresenterImpl(DatabaseRepository databaseRepository,
                                     SharedPreferencesRepository sharedPreferencesRepository,
                                     AnalyticsManager analyticsManager) {
        mDatabaseRepository = databaseRepository;
        mSharedPreferencesRepository = sharedPreferencesRepository;
        mAnalyticsManager = analyticsManager;
    }

    @Override
    public void bindView(SelectStocksView view) {
        mSelectView = view;
    }

    @Override
    public void initializeDataFromDatabase() {
        if(mSubscription != null && !mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }

        Observable<List<Stock>> stocks = mDatabaseRepository.queryStocks();
        mSubscription = stocks.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<Stock>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mSelectView.showMessage("An error has occurred.  Please try again.");
                    }

                    @Override
                    public void onNext(List<Stock> stocks) {
                        if(stocks.size() > 0) {
                            mSelectView.hideStocksLoading();
                        }
                        mSelectView.setStocks(stocks);
                    }
                });
    }

    @Override
    public void initializeSubscribedStocks() {
        final String[] subscribedStocks = ParseUtility.parseStockList(
                mSharedPreferencesRepository.getSubscribedStocks());
        mSelectView.setSubscribedStocks(subscribedStocks);
    }

    @Override
    public void saveSubscribedStocks(String[] stockSymbols) {
        String[] list = ParseUtility.parseStockList(mSharedPreferencesRepository.getSubscribedStocks());
        ArrayList<String> previousList = new ArrayList<>(Arrays.asList(list));
        ArrayList<String> currentList = new ArrayList<>(Arrays.asList(stockSymbols));
        List<String> toAppend = new ArrayList<>();

        // remove previous stocks, add to end of list.
        Iterator<String> iterator = currentList.iterator();
        while(iterator.hasNext()) {
            String currentStock = iterator.next();
            if(previousList.contains(currentStock)) {
                toAppend.add(currentStock);
                iterator.remove();
            }
            else {
                mAnalyticsManager.logSubscribeStock(currentStock);
            }
        }

        currentList.addAll(toAppend);
        mSharedPreferencesRepository.saveSubscribedStocks(currentList.toArray(new String[currentList.size()]));
    }

    @Override
    public void onActivityResume() {
        initializeDataFromDatabase();
        initializeSubscribedStocks();
    }

    @Override
    public void onActivityPause() {

    }

    @Override
    public void onActivityStop() {
        mSubscription.unsubscribe();
    }
}
