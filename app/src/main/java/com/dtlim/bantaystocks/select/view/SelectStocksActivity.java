package com.dtlim.bantaystocks.select.view;

import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dtlim.bantaystocks.BantayStocksApplication;
import com.dtlim.bantaystocks.BuildConfig;
import com.dtlim.bantaystocks.R;
import com.dtlim.bantaystocks.analytics.AnalyticsManager;
import com.dtlim.bantaystocks.data.database.repository.DatabaseRepository;
import com.dtlim.bantaystocks.data.model.Stock;
import com.dtlim.bantaystocks.data.repository.SharedPreferencesRepository;
import com.dtlim.bantaystocks.select.adapter.SelectStocksAdapter;
import com.dtlim.bantaystocks.select.presenter.SelectStocksPresenter;
import com.dtlim.bantaystocks.select.presenter.impl.SelectStocksPresenterImpl;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by dale on 7/4/16.
 */
public class SelectStocksActivity extends AppCompatActivity implements SelectStocksView {
    @BindView(R.id.bantaystocks_toolbar)
    Toolbar mToolbar;
    @BindView(R.id.bantaystocks_select_recyclerview)
    RecyclerView mRecyclerView;
    @BindView(R.id.bantaystocks_select_layout_loading)
    ViewGroup mLayoutLoading;
    @BindView(R.id.bantaystocks_select_adview)
    AdView mAdView;

    SearchView mSearchView;

    private SelectStocksAdapter mSelectAdapter;
    private SelectStocksPresenter mSelectPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bantaystocks_activity_select_stocks);
        ButterKnife.bind(this);

        initializeToolbar();
        initializeList();
        initializePresenter();
        initializeAds();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.select_stocks_menu, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mSelectAdapter.filter(newText);
                return false;
            }
        });
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mSelectPresenter != null) {
            mSelectPresenter.onActivityResume();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mSelectPresenter != null) {
            mSelectPresenter.onActivityStop();
        }
    }

    @Override
    public void onBackPressed() {
        mSelectPresenter.saveSubscribedStocks(mSelectAdapter.getSubscribedStocks());
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initializeToolbar() {
        if(mToolbar != null) {
            mToolbar.setTitle("Select stocks");
            setSupportActionBar(mToolbar);
            if(getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
        }
    }

    private void initializeList() {
        mSelectAdapter = new SelectStocksAdapter(this);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mSelectAdapter);
    }

    private void initializePresenter() {
        DatabaseRepository databaseRepository = BantayStocksApplication.getDatabaseRepository();
        SharedPreferencesRepository sharedPreferencesRepository =
                BantayStocksApplication.getSharedPreferencesRepository();
        AnalyticsManager analyticsManager = BantayStocksApplication.getAnalyticsManager();

        mSelectPresenter = new SelectStocksPresenterImpl(databaseRepository, sharedPreferencesRepository, analyticsManager);
        mSelectPresenter.bindView(this);
    }

    private void initializeAds() {
        MobileAds.initialize(this, BuildConfig.ADMOB_PUBLISHER_ID);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("4F03300E63121D77796B471B5A4DFC68") // cherry g1
                .addTestDevice("C1BACC051CC9F62EA4BA161FFD5B6D62") // shamesung SM-G313HZ
                .build();
        mAdView.loadAd(adRequest);
    }

    @Override
    public void setStocks(List<Stock> stocks) {
        mSelectAdapter.setStockList(stocks);
        if(mSearchView != null && mSearchView.getQuery() != null) {
            mSelectAdapter.filter(mSearchView.getQuery().toString());
        }
        else {
            mSelectAdapter.filter("");
        }
    }

    @Override
    public void setSubscribedStocks(String[] subscribedStocks) {
        mSelectAdapter.setSubscribedStocks(subscribedStocks);
    }

    @Override
    public void showStocksLoading() {
        mLayoutLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideStocksLoading() {
        mLayoutLoading.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }
}
