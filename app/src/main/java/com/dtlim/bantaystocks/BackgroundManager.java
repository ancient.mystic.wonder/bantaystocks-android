package com.dtlim.bantaystocks;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;
import java.lang.ref.WeakReference;

/**
 * Created by dale on 10/5/16.
 */

public class BackgroundManager {
    private WeakReference<Application> mApplication;
    private boolean mIsAppInBackground = false;

    private Handler mBackgroundCheckHandler;

    public BackgroundManager(Context context) {
        mApplication = new WeakReference<Application>((BantayStocksApplication) context.getApplicationContext());
        mBackgroundCheckHandler = new Handler();
        initializeActivityLifecycleCallbacks();
    }

    private void initializeActivityLifecycleCallbacks() {
        if(mApplication.get() != null) {
            mApplication.get().registerActivityLifecycleCallbacks(new Application.ActivityLifecycleCallbacks() {
                @Override
                public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                }

                @Override
                public void onActivityStarted(Activity activity) {
                }

                @Override
                public void onActivityResumed(Activity activity) {
                    mBackgroundCheckHandler.removeCallbacksAndMessages(null);
                    mIsAppInBackground = false;
                }

                @Override
                public void onActivityPaused(Activity activity) {
                    mIsAppInBackground = true;
                }

                @Override
                public void onActivityStopped(Activity activity) {
                    mBackgroundCheckHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if(mIsAppInBackground) {
                                Toast.makeText(mApplication.get(),
                                        "BantayStocks will keep running in the background.",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, 1000L);
                }

                @Override
                public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
                }

                @Override
                public void onActivityDestroyed(Activity activity) {
                }
            });
        }
    }

    public boolean isAppInBackground() {
        return mIsAppInBackground;
    }
}
