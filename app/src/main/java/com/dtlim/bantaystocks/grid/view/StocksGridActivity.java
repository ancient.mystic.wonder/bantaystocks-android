package com.dtlim.bantaystocks.grid.view;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.dtlim.bantaystocks.BantayStocksApplication;
import com.dtlim.bantaystocks.BuildConfig;
import com.dtlim.bantaystocks.R;
import com.dtlim.bantaystocks.data.model.Stock;
import com.dtlim.bantaystocks.grid.adapter.StocksGridAdapter;
import com.dtlim.bantaystocks.grid.presenter.StocksGridPresenter;
import com.dtlim.bantaystocks.grid.presenter.StocksGridPresenterImpl;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by amw on 10/2/2016.
 */
public class StocksGridActivity extends AppCompatActivity implements StocksGridView {

    @BindView(R.id.bantaystocks_toolbar)
    Toolbar mToolbar;
    @BindView(R.id.bantaystocks_stocks_grid_recyclerview)
    RecyclerView mRecyclerView;
    @BindView(R.id.bantaystocks_stocks_grid_layout_loading)
    ViewGroup mLayoutLoading;
    @BindView(R.id.bantaystocks_stocks_grid_adview)
    AdView mAdView;

    private StocksGridAdapter mStocksGridAdapter;

    private StocksGridPresenter mStocksGridPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bantaystocks_activity_stocks_grid);
        ButterKnife.bind(this);

        initializeToolbar();
        initializeRecyclerView();
        initializePresenter();
        initializeAds();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mStocksGridPresenter != null) {
            mStocksGridPresenter.onActivityPause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mStocksGridPresenter != null) {
            mStocksGridPresenter.onActivityResume();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mStocksGridPresenter != null) {
            mStocksGridPresenter.onActivityStop();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_enter_from_left, R.anim.activity_exit_to_right);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
//            super.onOptionsItemSelected(item);
//            overridePendingTransition(R.anim.activity_enter_from_right, R.anim.activity_exit_to_left);
        }
        return super.onOptionsItemSelected(item);
    }

    private void initializePresenter() {
        mStocksGridPresenter = new StocksGridPresenterImpl(BantayStocksApplication.getDatabaseRepository());
        mStocksGridPresenter.bindView(this);
        mStocksGridPresenter.initializeData();
    }

    private void initializeToolbar() {
        if(mToolbar != null) {
            mToolbar.setTitle("All stocks");
            setSupportActionBar(mToolbar);
            if(getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
        }
    }

    private void initializeRecyclerView() {
        mStocksGridAdapter = new StocksGridAdapter(this);
        mStocksGridAdapter.setHasStableIds(true);
        final GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 4);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mRecyclerView.setAdapter(mStocksGridAdapter);
    }

    private void initializeAds() {
        MobileAds.initialize(this, BuildConfig.ADMOB_PUBLISHER_ID);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("4F03300E63121D77796B471B5A4DFC68") // cherry g1
                .addTestDevice("C1BACC051CC9F62EA4BA161FFD5B6D62") // shamesung SM-G313HZ
                .build();
        mAdView.loadAd(adRequest);
    }

    @Override
    public void setStockList(List<Stock> stockList) {
        mStocksGridAdapter.setStockList(stockList);
    }

    @Override
    public void showStocksLoading() {
        mLayoutLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideStocksLoading() {
        mLayoutLoading.setVisibility(View.GONE);
    }
}
