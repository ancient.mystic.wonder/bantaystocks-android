package com.dtlim.bantaystocks.grid.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dtlim.bantaystocks.R;
import com.dtlim.bantaystocks.data.model.Stock;
import com.dtlim.bantaystocks.dummy.DummyModels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by amw on 10/2/2016.
 */
public class StocksGridAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Stock> mListStocks;
    private Context mContext;

    public StocksGridAdapter(Context context) {
        mContext = context;
        mListStocks = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.bantaystocks_stocks_grid_item, parent, false);
        return new StocksGridItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        bindStocksGridItemViewHolder((StocksGridItemViewHolder) holder, position);
    }

    @Override
    public int getItemCount() {
        return mListStocks.size();
    }

    @Override
    public long getItemId(int position) {
        return mListStocks.get(position).getSymbol().hashCode();
    }

    public void setStockList(List<Stock> stockList) {
        mListStocks = stockList;
        notifyDataSetChanged();
    }

    private void bindStocksGridItemViewHolder(StocksGridItemViewHolder holder, int position) {
        Stock stock = mListStocks.get(position);
        holder.textViewSymbol.setText(stock.getSymbol());
        holder.textViewPrice.setText(stock.getAmount());
        holder.textViewPercentChange.setText(stock.getPercentChange() + "%");

        try {
            float percentage = Float.valueOf(stock.getPercentChange());
            if (percentage > 0) {
                holder.cardView.setCardBackgroundColor(
                        ContextCompat.getColor(mContext, R.color.bantaystocks_color_stock_price_up_green));
                holder.textViewSymbol.setTextColor(
                        ContextCompat.getColor(mContext, R.color.bantaystocks_color_home_background));
                holder.textViewPrice.setTextColor(
                        ContextCompat.getColor(mContext, R.color.bantaystocks_color_home_background));
                holder.textViewPercentChange.setTextColor(
                        ContextCompat.getColor(mContext, R.color.bantaystocks_color_home_background));
            }
            else if (percentage < 0) {
                holder.cardView.setCardBackgroundColor(
                        ContextCompat.getColor(mContext, R.color.bantaystocks_color_stock_price_down_red));
                holder.textViewSymbol.setTextColor(
                        ContextCompat.getColor(mContext, R.color.bantaystocks_color_home_background));
                holder.textViewPrice.setTextColor(
                        ContextCompat.getColor(mContext, R.color.bantaystocks_color_home_background));
                holder.textViewPercentChange.setTextColor(
                        ContextCompat.getColor(mContext, R.color.bantaystocks_color_home_background));
            }
            else {
                holder.cardView.setCardBackgroundColor(
                        ContextCompat.getColor(mContext, android.R.color.white));
                holder.textViewSymbol.setTextColor(
                        ContextCompat.getColor(mContext, R.color.bantaystocks_color_home_text));
                holder.textViewPrice.setTextColor(
                        ContextCompat.getColor(mContext, R.color.bantaystocks_color_home_text));
                holder.textViewPercentChange.setTextColor(
                        ContextCompat.getColor(mContext, R.color.bantaystocks_color_home_text));
            }
        }
        catch(NumberFormatException e) {
            holder.textViewPrice.setTextColor(
                    ContextCompat.getColor(mContext, R.color.bantaystocks_color_home_text));
            holder.textViewPercentChange.setTextColor(
                    ContextCompat.getColor(mContext, R.color.bantaystocks_color_home_text));
        }
    }

    static class StocksGridItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.bantaystocks_stocks_grid_item_cardview)
        public CardView cardView;
        @BindView(R.id.bantaystocks_stocks_grid_item_symbol)
        public TextView textViewSymbol;
        @BindView(R.id.bantaystocks_stocks_grid_item_price)
        public TextView textViewPrice;
        @BindView(R.id.bantaystocks_stocks_grid_item_percent_change)
        public TextView textViewPercentChange;

        public StocksGridItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
