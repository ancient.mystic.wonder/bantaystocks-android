package com.dtlim.bantaystocks.grid.presenter;

import com.dtlim.bantaystocks.base.BasePresenter;
import com.dtlim.bantaystocks.grid.view.StocksGridView;

/**
 * Created by dale on 10/3/16.
 */

public interface StocksGridPresenter extends BasePresenter<StocksGridView> {
    void initializeData();
}
