package com.dtlim.bantaystocks.grid.presenter;

import android.util.Log;

import com.dtlim.bantaystocks.data.database.repository.DatabaseRepository;
import com.dtlim.bantaystocks.data.model.Stock;
import com.dtlim.bantaystocks.grid.view.StocksGridView;

import java.util.List;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by dale on 10/3/16.
 */

public class StocksGridPresenterImpl implements StocksGridPresenter {

    private StocksGridView mStocksGridView;
    private DatabaseRepository mDatabaseRepository;

    private Subscription mSubscription;

    public StocksGridPresenterImpl(DatabaseRepository databaseRepository) {
        mDatabaseRepository = databaseRepository;
    }

    @Override
    public void bindView(StocksGridView view) {
        mStocksGridView = view;
    }

    @Override
    public void onActivityResume() {

    }

    @Override
    public void onActivityPause() {

    }

    @Override
    public void onActivityStop() {
        if(mSubscription != null && !mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
    }

    @Override
    public void initializeData() {

        if(mSubscription != null && !mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }

        mSubscription = mDatabaseRepository.queryStocks()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<Stock>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(List<Stock> stocks) {
                if(stocks.size() > 0) {
                    mStocksGridView.hideStocksLoading();
                    mStocksGridView.setStockList(stocks);
                }
            }
        });
    }
}
