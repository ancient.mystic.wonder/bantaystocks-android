package com.dtlim.bantaystocks.grid.view;

import com.dtlim.bantaystocks.data.model.Stock;

import java.util.List;

/**
 * Created by amw on 10/2/2016.
 */
public interface StocksGridView {
    void setStockList(List<Stock> stocks);
    void showStocksLoading();
    void hideStocksLoading();
}
