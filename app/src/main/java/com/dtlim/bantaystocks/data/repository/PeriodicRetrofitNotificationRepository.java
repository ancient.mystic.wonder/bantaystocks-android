package com.dtlim.bantaystocks.data.repository;

import com.dtlim.bantaystocks.BuildConfig;
import com.dtlim.bantaystocks.data.api.StocksApiInterface;
import com.dtlim.bantaystocks.data.model.Stock;

import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by dale on 7/15/16.
 */
public class PeriodicRetrofitNotificationRepository implements StocksNotificationRepository {

    private static final int SUCCESSFUL_QUERY_INTERVAL = BuildConfig.STOCKS_API_SUCCESS_REPEAT_INTERVAL;

    private StocksApiInterface mStocksApiInterface;

    public PeriodicRetrofitNotificationRepository() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor()
                .setLevel(BuildConfig.DEBUG ?
                        HttpLoggingInterceptor.Level.BASIC :
                        HttpLoggingInterceptor.Level.NONE);
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .build();
        RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io());
        mStocksApiInterface = new Retrofit.Builder()
                .client(okHttpClient)
                .addCallAdapterFactory(rxAdapter)
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BuildConfig.STOCKS_API_BASE_URL)
                .build()
                .create(StocksApiInterface.class);
    }

    @Override
    public void connect(ConnectionListener listener) throws Throwable {
        listener.onConnectSuccess();
    }

    @Override
    public Observable<List<Stock>> getStocks() {
       return mStocksApiInterface.getStocks()
                .repeatWhen(new Func1<Observable<? extends Void>, Observable<?>>() {
                    @Override
                    public Observable<?> call(final Observable<? extends Void> observable) {
                        return observable.delay(SUCCESSFUL_QUERY_INTERVAL, TimeUnit.SECONDS);
                    }
                });
    }
}
