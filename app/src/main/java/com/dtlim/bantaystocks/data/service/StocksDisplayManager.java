package com.dtlim.bantaystocks.data.service;

import android.content.Context;
import android.graphics.PixelFormat;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.dtlim.bantaystocks.common.utility.ParseUtility;
import com.dtlim.bantaystocks.data.database.repository.DatabaseRepository;
import com.dtlim.bantaystocks.data.model.Stock;
import com.dtlim.bantaystocks.data.repository.SharedPreferencesRepository;
import com.dtlim.bantaystocks.home.customview.HomescreenItemTouchListener;
import com.dtlim.bantaystocks.home.customview.HomescreenStockItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by amw on 9/10/2016.
 */
public class StocksDisplayManager implements SharedPreferencesRepository.Listener,
        HomescreenStockItem.HomescreenStockItemListener{

    private Context mContext;
    private WindowManager mWindowManager;
    private DatabaseRepository mDatabaseRepository;
    private SharedPreferencesRepository mSharedPreferencesRepository;

    private HashMap<String, HomescreenStockItem> mStockItems;
    private List<Stock> mStocks = new ArrayList<>();

    public StocksDisplayManager(Context context, DatabaseRepository databaseRepository, SharedPreferencesRepository sharedPreferencesRepository) {
        mContext = context;
        mDatabaseRepository = databaseRepository;
        mSharedPreferencesRepository = sharedPreferencesRepository;
        initialize();
    }

    private void initialize() {
        mWindowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        mStockItems = new HashMap<>();

        Observable<List<Stock>> stocksObservable = mDatabaseRepository.queryStocks();
        stocksObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<Stock>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<Stock> stocks) {
                        if(stocks != null && !stocks.isEmpty()) {
                            mStocks = stocks;
                            updateHomeStocksView(stocks);
                        }
                    }
                });

        mSharedPreferencesRepository.registerSharedPreferencesListener(this);
    }

    @Override
    public void onCloseButtonClick(Stock stock) {
        String[] list = ParseUtility.parseStockList(mSharedPreferencesRepository.getWatchedStocks());
        ArrayList<String> stocks = new ArrayList<>(Arrays.asList(list));
        stocks.remove(stock.getSymbol());
        mSharedPreferencesRepository.saveWatchedStocks(stocks.toArray(new String[stocks.size()]));
    }

    @Override
    public void onPreferenceChanged() {
        String watchedStocksPrefs = mSharedPreferencesRepository.getWatchedStocks();
        List<String> watchedStocks = TextUtils.isEmpty(watchedStocksPrefs) ? new ArrayList<String>()
                : Arrays.asList(ParseUtility.parseStockList(watchedStocksPrefs));
        Collection<String> hashMapKeys = mStockItems.keySet();

        // Add or show stocks in prefs
        for(String watchedStock : watchedStocks) {
            if(!hashMapKeys.contains(watchedStock) || mStockItems.get(watchedStock) == null) {
                HomescreenStockItem item = createHomescreenStockView();
                mStockItems.put(watchedStock, item);
            }
            mStockItems.get(watchedStock).show();
        }

        // Hide stocks not in prefs
        for(String key : mStockItems.keySet()) {
            if(!watchedStocks.contains(key) && mStockItems.get(key) != null) {
                mStockItems.get(key).hide();
            }
        }

        updateHomeStocksView(mStocks);
    }

    public boolean isDisplayingStocks() {
        for(HomescreenStockItem item : mStockItems.values()) {
            if(item.getVisibility() == View.VISIBLE) {
                return true;
            }
        }
        return false;
    }

    private void updateHomeStocksView(List<Stock> stocks) {
        for(int i=0; i<stocks.size(); i++) {
            Stock currentStock = stocks.get(i);
            if(mStockItems.get(currentStock.getSymbol()) != null) {
                HomescreenStockItem currentItem = mStockItems.get(currentStock.getSymbol());
                currentItem.setStock(currentStock);
            }
        }
    }

    private HomescreenStockItem createHomescreenStockView() {
        HomescreenStockItem stockItem = new HomescreenStockItem(mContext);
        stockItem.setHomescreenStockItemListener(this);

        final WindowManager.LayoutParams params= new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);
        params.gravity = Gravity.TOP | Gravity.START;
        params.x = 0;
        params.y = 100;

        // Have to add another view to enable animations in WindowManager for pre-lollipop
        // http://stackoverflow.com/questions/17745282/windowmanager-with-animation-is-it-possible
        FrameLayout mLayout = new FrameLayout(mContext);
        stockItem.setOnTouchListener(new HomescreenItemTouchListener(mWindowManager, mLayout, params));
        mLayout.addView(stockItem);

        mWindowManager.addView(mLayout, params);
        return stockItem;
    }
}
