package com.dtlim.bantaystocks.data.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.Toast;

import com.dtlim.bantaystocks.BantayStocksApplication;
import com.dtlim.bantaystocks.BuildConfig;
import com.dtlim.bantaystocks.R;
import com.dtlim.bantaystocks.common.utility.DateUtility;
import com.dtlim.bantaystocks.data.database.repository.DatabaseRepository;
import com.dtlim.bantaystocks.data.model.Stock;
import com.dtlim.bantaystocks.data.repository.SharedPreferencesRepository;
import com.dtlim.bantaystocks.data.repository.StocksNotificationRepository;
import com.dtlim.bantaystocks.home.view.HomeActivity;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by dale on 6/23/16.
 */
public class StocksNotificationService extends Service implements StocksNotificationRepository.ConnectionListener {

    public static String INTENT_ACTION_CLOSE_PROCESS = "BantayStocks close process";

    private StocksNotificationRepository mStocksNotificationRepository = BantayStocksApplication.getStocksNotificationRepository();
    private DatabaseRepository mDatabaseRepository = BantayStocksApplication.getDatabaseRepository();
    private SharedPreferencesRepository mSharedPreferencesRepository =
            BantayStocksApplication.getSharedPreferencesRepository();
    private StocksDisplayManager mStocksDisplayManager;

    private BroadcastReceiver mBroadcastReceiver;

    private static final int RETRY_INTERVAL = BuildConfig.STOCKS_API_FAIL_RETRY_INTERVAL;

    public void onCreate() {
        super.onCreate();
        mStocksDisplayManager = new StocksDisplayManager(this, mDatabaseRepository, mSharedPreferencesRepository);
        initialize();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void initialize() {
        initializeBroadcastReceiver();
        initializeNotification();
        try {
            mStocksNotificationRepository.connect(this);
        }
        catch (Throwable e) {
            Timber.e(e, null);
        }
    }

    private void initializeNotification() {
        Intent closeIntent = new Intent();
        closeIntent.setAction(INTENT_ACTION_CLOSE_PROCESS);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, closeIntent, 0);
        NotificationCompat.Action action = new NotificationCompat.Action(
                R.drawable.bantaystocks_icon_notification_close, "Close", pendingIntent);

        Intent openIntent = new Intent(this, HomeActivity.class);
        PendingIntent openPendingIntent = PendingIntent.getActivity(this, 0, openIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("BantayStocks")
                .setContentText("BantayStocks is currently running.")
                .setContentIntent(openPendingIntent)
                .setOngoing(true)
                .addAction(action)
                .build();
        notificationManager.notify(9999, notification);
    }

    private void initializeBroadcastReceiver() {
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (action.equals(INTENT_ACTION_CLOSE_PROCESS)) {
                    closeProcess();
                }
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction(INTENT_ACTION_CLOSE_PROCESS);

        registerReceiver(mBroadcastReceiver, filter);
    }

    private void saveStocksToDb(List<Stock> stocks) {
        long lastUpdate = mSharedPreferencesRepository.getLastUpdate();
        long currentUpdate = 0;
        try {
            currentUpdate = Long.valueOf(
                    DateUtility.parseApiToUnixTimestamp(stocks.get(0).getLastUpdate()));
        }
        catch (Exception e) {

        }
        Log.d("SAVES", "saveStocksToDb: " + lastUpdate + " " + currentUpdate);
        if(currentUpdate > lastUpdate) {
            long rowsUpdated = mDatabaseRepository.insert(stocks);
            if(rowsUpdated > 0) {
                Log.d("SAVES", "saveStocksToDb: update");
                mSharedPreferencesRepository.saveLastUpdate(currentUpdate);
            }
        }
    }

    private void closeProcess() {
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.cancelAll();
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    @Override
    public void onConnectSuccess() {
        try {
            mStocksNotificationRepository.getStocks()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnError(new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {
                            EventBus.getDefault().post(throwable);
                        }
                    })
                    .retryWhen(new Func1<Observable<? extends Throwable>, Observable<?>>() {
                        @Override
                        public Observable<?> call(Observable<? extends Throwable> observable) {
                            return observable.delay(RETRY_INTERVAL, TimeUnit.SECONDS);
                        }
                    })
                    .subscribe(new Subscriber<List<Stock>>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            EventBus.getDefault().post(e);
                        }

                        @Override
                        public void onNext(List<Stock> stocks) {
                            if(stocks != null && stocks.size() > 0) {
                                saveStocksToDb(stocks);
                            }
                        }
                    });
        }
        catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectFail() {

    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.cancelAll();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "DESTROY SERVICE", Toast.LENGTH_SHORT).show();
    }
}
