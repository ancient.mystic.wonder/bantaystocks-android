package com.dtlim.bantaystocks.data.database.dao;

import android.content.ContentValues;
import android.database.Cursor;

import com.dtlim.bantaystocks.common.utility.DateUtility;
import com.dtlim.bantaystocks.data.database.Database;
import com.dtlim.bantaystocks.data.database.table.StockTable;
import com.dtlim.bantaystocks.data.model.Stock;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dale on 6/29/16.
 */
public class StockDao extends BaseDao<Stock>{

    public StockDao(Database database) {
        super(database);
    }

    @Override
    protected String getTableName() {
        return StockTable.TABLE_NAME;
    }

    @Override
    protected ContentValues getContentValues(Stock stock) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(StockTable.NAME, stock.getName());
        contentValues.put(StockTable.AMOUNT, stock.getAmount());
        contentValues.put(StockTable.CURRENCY, stock.getCurrency());
        contentValues.put(StockTable.PERCENT_CHANGE, stock.getPercentChange());
        contentValues.put(StockTable.VOLUME, stock.getVolume());
        contentValues.put(StockTable.SYMBOL, stock.getSymbol());
        contentValues.put(StockTable.LAST_UPDATE, stock.getLastUpdate());

        return contentValues;
    }

    @Override
    public List<Stock> parseCursor(Cursor cursor) {
        List<Stock> stockList = new ArrayList<>();

        try{
            if(cursor != null && cursor.moveToFirst()) {
                do {
                    Stock stock = new Stock();
                    stock.setName(cursor.getString(cursor.getColumnIndex(StockTable.NAME)));
                    stock.setPercentChange(cursor.getString(cursor.getColumnIndex(StockTable.PERCENT_CHANGE)));
                    stock.setVolume(cursor.getString(cursor.getColumnIndex(StockTable.VOLUME)));
                    stock.setSymbol(cursor.getString(cursor.getColumnIndex(StockTable.SYMBOL)));
                    stock.setCurrency(cursor.getString(cursor.getColumnIndex(StockTable.CURRENCY)));
                    stock.setAmount(cursor.getString(cursor.getColumnIndex(StockTable.AMOUNT)));
                    stock.setLastUpdate(DateUtility.parseApiToUnixTimestamp(
                            cursor.getString(cursor.getColumnIndex(StockTable.LAST_UPDATE))));
                    stockList.add(stock);
                }
                while (cursor.moveToNext());
            }
        }
        finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return stockList;
    }
}
