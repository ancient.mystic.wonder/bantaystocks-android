package com.dtlim.bantaystocks.analytics;

import java.util.List;
import java.util.Map;

/**
 * Created by amw on 9/2/2016.
 */
public interface AnalyticsManager {
    void logCustomEvent(String name, Map<String, String> params);
    void logSubscribeStocks(List<String> stockSymbols);
    void logSubscribeStock(String stockSymbol);
}
