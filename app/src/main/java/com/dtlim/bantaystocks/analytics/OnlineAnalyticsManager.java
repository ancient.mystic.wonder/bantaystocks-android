package com.dtlim.bantaystocks.analytics;

import android.content.Context;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by amw on 9/2/2016.
 */
public class OnlineAnalyticsManager implements AnalyticsManager {

    public static final String SUBSCRIBE_STOCK_EVENT = "Subscribe";
    public static final String PARAMETER_KEY_STOCK_SYMBOL = "Stock symbol";
    FirebaseAnalytics mFirebaseAnalytics;

    public OnlineAnalyticsManager(Context context) {
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
    }

    @Override
    public void logCustomEvent(String name, Map<String, String> params) {
        Bundle bundle = new Bundle();
        for(String key : params.keySet()) {
            bundle.putString(key, params.get(key));
        }
        mFirebaseAnalytics.logEvent(name, bundle);
    }

    @Override
    public void logSubscribeStock(String stockSymbol) {
        Map<String, String> map = new HashMap<>();
        map.put(PARAMETER_KEY_STOCK_SYMBOL, stockSymbol);
        logCustomEvent(SUBSCRIBE_STOCK_EVENT, map);
    }

    @Override
    public void logSubscribeStocks(List<String> stockSymbols) {
        for(String stock : stockSymbols) {
            logSubscribeStock(stock);
        }
    }
}
